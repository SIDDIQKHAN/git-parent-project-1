import morgan from 'morgan';

import { LoggerTraceability } from '@common/Logger';
const logger = LoggerTraceability.getInstance().getLogger();

export { logRequest, logResponse };

const logRequest = morgan('Request received for :method uri :url HTTP/:http-version', {
    immediate: true,
    stream: {
        write: (message) => {
            logger.info(message.trim());
        },
    },
});

const logResponse = morgan('Response completed with status :status for :method uri :url in :total-time[2] ms', {
    stream: {
        write: (message) => {
            logger.info(message.trim());
        },
    },
});
