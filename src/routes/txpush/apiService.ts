import {moduleFunction} from '../../../sub-module/src/clients/module'

/**
 * Method to create txpush events.
 * @param eventObjectList event object
 * @returns
 */
export async function createEvents() {
    const result = await moduleFunction();
    return result;
}