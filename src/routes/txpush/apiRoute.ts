import finicityExpress, { NextFunction, Request, Response } from 'express';
import httpStatus from 'http-status';

import * as txpushService from '@txpush-route/apiService'

const txpushRouter = finicityExpress.Router();

export { txpushRouter };

txpushRouter.get('/get', async (req: Request, res: Response, next: NextFunction) => {
    try {
        const result = await txpushService.createEvents();
        res.status(httpStatus.CREATED).json({response: result, message: 'Events created successfully' });
    } catch (err) {
        next(err);
    }
});
