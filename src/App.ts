process.env.NODE_ENV = process.env.NODE_ENV || 'local-dev';
process.env.NODE_CONFIG_DIR = __dirname + '/config';

import axios from 'axios';
import express, { NextFunction, Request, Response } from 'express';
import userAgent from 'express-useragent';
import helmet from 'helmet';
import showBanner from 'node-banner';

import { reqInterceptor, reqInterceptorError, resInterceptor, resInterceptorError } from './middleware/ClientMiddleware';

import { errorHandler } from '@common/errors';
import { ContextAsyncHooks } from '@common/hooks/ContextAsyncHooks';
import { LoggerTraceability } from '@common/Logger';

import { logRequest, logResponse } from '@middleware/ApiLoggingMiddleware';
import { txpushRouter } from '@txpush-route/apiRoute';

const passportEvents = express();
const Logger = LoggerTraceability.getInstance().getLogger();
passportEvents.use(ContextAsyncHooks.getExpressMiddlewareTracking());

const PORT = 3000;
const applicationName = 'parent';

passportEvents.listen(PORT, async () => {
    await showBanner('Sky Passport Events', `Started at port ${PORT}`);
});

passportEvents.use(express.json());
passportEvents.use(helmet());
passportEvents.use(userAgent.express());

axios.interceptors.request.use(reqInterceptor, reqInterceptorError);
axios.interceptors.response.use(resInterceptor, resInterceptorError);

if (process.env.NODE_ENV === 'testing') passportEvents.use(logRequest, logResponse);
else passportEvents.use(logRequest, logResponse);

passportEvents.use(`/${applicationName}/1`, txpushRouter);

passportEvents.use(async (error: Error, req: Request, res: Response, next: NextFunction) => {
    await errorHandler.handleError(error, res);
    if (next) {
        next();
    }
});

Logger.info(`Application running with ${process.env.NODE_ENV} env config`);
Logger.info('Application is starting!!');

export default passportEvents;
