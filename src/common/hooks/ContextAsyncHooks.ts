/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-disable no-console */
/* eslint-disable class-methods-use-this */
import { AsyncLocalStorage } from 'async_hooks';
import crypto from 'crypto';
import { NextFunction, Request, Response } from 'express';

export interface RequestContext {
    [key: string]: string | string[] | undefined;
}

export enum ETrackKey {
    'X-Request-ID' = 'X-Request-ID',
    'X-Correlation-ID' = 'X-Correlation-ID',
    'traceId' = 'traceId',
    'requestId' = 'requestId',
}
class ContextAsyncHooksClass {
    private static instance: ContextAsyncHooksClass;

    public asyncLocalStorage: AsyncLocalStorage<any>;

    public traceKey: ETrackKey = ETrackKey.traceId;

    public requestKey: ETrackKey = ETrackKey.requestId;

    private constructor() {
        this.asyncLocalStorage = new AsyncLocalStorage<any>();
    }

    public getExpressMiddlewareTracking() {
        return (request: Request, response: Response, next: NextFunction): void => {
            const context = this.getTraceAndRequestId();
            this.setContext(context);
            response.setHeader(this.traceKey, context[this.traceKey] as string);
            next();
        };
    }

    public setContext(data: RequestContext): void {
        let oldData = this.getContext();
        oldData = { ...oldData, ...data };
        this.asyncLocalStorage.enterWith(oldData);
    }

    public getTraceAndRequestId(): RequestContext {
        // eslint-disable-next-line prefer-object-spread
        const logInfo = Object.assign({}, this.traceKey) as any;
        const finalLogInfo = Object.assign(logInfo, this.requestKey) as any;

        const tId = crypto.randomBytes(8).toString('hex');
        finalLogInfo[this.traceKey] = tId;

        const requestId = Math.floor(Math.random() * (999999999 - 100 + 1) + 100);
        finalLogInfo[this.requestKey] = `FIN${requestId}`;

        return finalLogInfo;
    }

    public getContext(): RequestContext | undefined {
        return this.asyncLocalStorage.getStore();
    }

    public static getInstance(): ContextAsyncHooksClass {
        if (!ContextAsyncHooksClass.instance) {
            ContextAsyncHooksClass.instance = new ContextAsyncHooksClass();
        }
        return ContextAsyncHooksClass.instance;
    }
}

export const ContextAsyncHooks = ContextAsyncHooksClass.getInstance();
