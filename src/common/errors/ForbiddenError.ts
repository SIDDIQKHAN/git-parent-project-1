import { BaseError, HttpStatusCode } from './BaseError';
export class ForbiddenError extends BaseError {
    constructor(description = 'Forbidden') {
        super('Forbidden', HttpStatusCode.FORBIDDEN, description, true);
    }
}
