import { Response } from 'express';

import { AuthenticationError, BadRequest, ServerError } from './';
import { ForbiddenError } from './ForbiddenError';
import { TooManyRequestsError } from './TooManyRequestsError';

import { InsufficientDataError } from '@common/errors/InsufficientDataError';
import { LoggerTraceability } from '@common/Logger';

const logger = LoggerTraceability.getInstance().getLogger();

class ErrorHandler {
    async handleError(err: Error, res: Response): Promise<void> {
        let error;
        if (err instanceof BadRequest) {
            error = err as BadRequest;
        } else if (err instanceof AuthenticationError) {
            error = err as AuthenticationError;
        } else if (err instanceof ForbiddenError) {
            error = err as ForbiddenError;
        } else if (err instanceof TooManyRequestsError) {
            error = err as TooManyRequestsError;
        } else if (err instanceof ServerError) {
            error = err as ServerError;
        } else if (err instanceof InsufficientDataError) {
            error = err as InsufficientDataError;
        } else {
            logger.error(err);
            error = { httpCode: 500, name: 'Internal Server Error', message: 'Unable to process this request!!' };
        }

        res.setHeader('Strict-Transport-Security', 'max-age=31536000; includeSubDomains; preload');
        res.status(error.httpCode);
        res.send({ error: error.name, message: error.message });
    }
}
export const errorHandler = new ErrorHandler();
