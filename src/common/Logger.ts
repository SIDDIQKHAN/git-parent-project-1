/* eslint-disable class-methods-use-this */
/* eslint-disable no-param-reassign */
import { addColors, createLogger, format, Logger, LoggerOptions, transports } from 'winston';

// winston logform type
// import { TransformableInfo } from 'logform';
import { ContextAsyncHooks } from './hooks/ContextAsyncHooks';

const colors = {
    error: 'red',
    warn: 'yellow',
    info: 'green',
    http: 'magenta',
    debug: 'white',
};

addColors(colors);

export class LoggerTraceability {
    private static instance: LoggerTraceability;

    private logger: Logger;

    private constructor() {
        this.logger = createLogger(LoggerTraceability.getLoggerOptions());
    }

    public static getInstance(): LoggerTraceability {
        if (!LoggerTraceability.instance) {
            LoggerTraceability.instance = new LoggerTraceability();
        }
        return LoggerTraceability.instance;
    }

    public static configure(options: LoggerOptions): void {
        LoggerTraceability.getInstance().getLogger().configure(options);
    }

    public static getLoggerOptions(): LoggerOptions {
        const traceFormat = format.printf((info) => {
            const requestInfo = ContextAsyncHooks.getContext();
            if (requestInfo) {
                if (requestInfo[ContextAsyncHooks.traceKey]) {
                    info[ContextAsyncHooks.traceKey] = requestInfo[ContextAsyncHooks.traceKey];
                }

                if (requestInfo[ContextAsyncHooks.requestKey]) {
                    info[ContextAsyncHooks.requestKey] = requestInfo[ContextAsyncHooks.requestKey];
                }
            }

            // don't show [undefined] for app starting messages
            const showTrackId =
                !(info.message === 'Application is starting!!') &&
                !info.message.includes('Application started') &&
                !info.message.startsWith('Application running with');
            const tracer = showTrackId ? ` : [traceId : ${info['traceId']}] [requestId : ${info['requestId']}]` : '';

            return `FIN [SKYW] [${info.timestamp}]${tracer} [${info.level}: ${info.message}] ${info.stack ? '\n' + info.stack : ''}`;
        });

        return {
            level: level(),
            levels,
            silent: process.env.NODE_ENV === 'testing' ? true : false,
            format: format.combine(
                format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss:ms' }),
                traceFormat,
                format.metadata({ fillExcept: ['timestamp', 'service', 'level', 'message'] }),
                format.colorize({ all: true })
                // format.json()
            ),
            transports: [new transports.Console()],
        };
    }

    public getLogger(): Logger {
        return this.logger;
    }
}

const level = () => {
    const env = process.env.NODE_ENV || 'development';
    const isDevelopment = env === 'development' || env === 'local-dev';
    return isDevelopment ? 'debug' : 'info';
};

const levels = {
    error: 0,
    warn: 1,
    info: 2,
    http: 3,
    debug: 4,
};
